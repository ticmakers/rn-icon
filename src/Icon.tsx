import * as React from 'react'
import { StyleSheet } from 'react-native'
import { Icon as RNE_Icon } from 'react-native-elements'

import { IIconProps, IIconStates } from './../index.d'
import styles from './styles'

/**
 * Class to define the component Icon
 * @class Icon
 * @extends {React.Component<IIconProps, IIconStates>}
 */
export default class Icon extends React.Component<IIconProps, IIconStates> {
  constructor(props: IIconProps) {
    super(props)
    this.state = this._processProps()
  }

  /**
   * Method that renders the component
   * @returns {(JSX.Element | React.ComponentClass)}
   * @memberof Icon
   */
  public render(): JSX.Element | React.ComponentClass {
    const { containerStyle, disabledStyle, iconStyle, onLongPress, onPress } = this._processProps()

    return (
      <RNE_Icon
        {...this._processProps()}
        containerStyle={ StyleSheet.flatten([styles.containerStyle, containerStyle]) }
        disabledStyle={ StyleSheet.flatten([styles.disabledStyle, disabledStyle]) }
        iconStyle={ StyleSheet.flatten([styles.iconStyle, iconStyle]) }
        onLongPress={ onLongPress && (() => this._onLongPress()) }
        onPress={ onPress && (() => this._onPress()) }
      />
    )
  }

  /**
   * Method that fire when the icon is long pressed
   * @private
   * @returns {void}
   * @memberof Icon
   */
  private _onLongPress(): void {
    const { onLongPress } = this._processProps()

    if (onLongPress) {
      return onLongPress()
    }
  }

  /**
   * Method that fire when the icon is pressed
   * @private
   * @returns {void}
   * @memberof Icon
   */
  private _onPress(): void {
    const { onPress } = this._processProps()

    if (onPress) {
      return onPress()
    }
  }

  /**
   * Method that process the props
   * @private
   * @returns {IIconProps}
   * @memberof Icon
   */
  private _processProps(): IIconProps {
    const { color, Component, containerStyle, disabled, disabledStyle, iconStyle, name, onLongPress, onPress, options, raised, reverse, reverseColor, size, type, underlayColor } = this.props

    const params: IIconProps = {
      Component: (options && options.Component) || (Component || undefined),
      color: (options && options.color) || (color || 'black'),
      containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
      disabled: (options && options.disabled) || (disabled || false),
      disabledStyle: (options && options.disabledStyle) || (disabledStyle || undefined),
      iconStyle: (options && options.iconStyle) || (iconStyle || undefined),
      name: (options && options.name) || (name || ''),
      onLongPress: (options && options.onLongPress) || (onLongPress || undefined),
      onPress: (options && options.onPress) || (onPress || undefined),
      raised: (options && options.raised) || (raised || false),
      reverse: (options && options.reverse) || (reverse || false),
      reverseColor: (options && options.reverseColor) || (reverseColor || 'white'),
      size: (options && options.size) || (size || 26),
      type: (options && options.type) || (type || 'material'),
      underlayColor: (options && options.underlayColor) || (underlayColor || 'black'),
    }

    return params
  }
}
