import * as React from 'react'
import {
  RecursiveArray,
  RegisteredStyle,
  StyleProp,
  ViewStyle,
} from 'react-native'

/**
 * Set icons that can use in the Icon component
 */
export type TypeIcons =
  'antdesign'
  | 'entypo'
  | 'evilicon'
  | 'feather'
  | 'font-awesome'
  | 'foundation'
  | 'ionicon'
  | 'material'
  | 'material-community'
  | 'octicon'
  | 'rubicon-icon-font'
  | 'simple-line-icon'
  | 'zocial'

/**
 * Type to define the prop style of the Icon component
 */
export type TypeStyle =
  false
  | ViewStyle
  | RegisteredStyle<ViewStyle>
  | RecursiveArray<false | ViewStyle | RegisteredStyle<ViewStyle> | null | undefined>
  | StyleProp<ViewStyle>[]
  | null
  | undefined

/**
 * Interface to define the states of the Icon component
 * @export
 * @interface IIconStates
 */
export interface IIconStates {
  /**
   * Color of the icon (hex,rgb,etc...)
   * @default black
   */
  color?: string

  /**
   * Add styling to container holding the icon
   */
  containerStyle?: TypeStyle

  /**
   * Replace the component to show the icon
   */
  Component?: React.ComponentClass

  /**
   * Disables onPress events. Only works when `onPress` has a handler
   * @default false
   */
  disabled?: boolean

  /**
   * Style for the button when disabled. Only works when `onPress` has a handler
   * @default {{backgroundColor: '#D1D5D8'}}
   */
  disabledStyle?: TypeStyle

  /**
   * Additional styling to the icon
   */
  iconStyle?: TypeStyle

  /**
   * Name of the icon
   */
  name: string

  /**
   * Method that fire when the icon is pressed
   */
  onPress?: () => void

  /**
   * Method that fire when the icon is long pressed
   */
  onLongPress?: () => void

  /**
   * Adds box shadow to container
   * @default false
   */
  raised?: boolean

  /**
   * Reverses color scheme
   * @default false
   */
  reverse?: boolean

  /**
   * Specify reverse icon color
   * @default white
   */
  reverseColor?: string

  /**
   * Size of the icon
   * @default 26
   */
  size?: number

  /**
   * Type of icon set
   * @default material
   */
  type?: TypeIcons

  /**
   * Underlay color for press event
   * @default black
   */
  underlayColor?: string
}

/**
 * Interface to define the props of the Icon component
 * @export
 * @interface IIconProps
 * @extends {IIconStates}
 */
export interface IIconProps extends IIconStates {
  /**
   * Prop for group all the props of the Icon component
   */
  options?: IIconStates
}


/**
 * Declaration for Icon component
 * @class Icon
 * @extends {React.Component<IIconProps, IIconStates>}
 */
declare class Icon extends React.Component<IIconProps, IIconStates> {
}

/**
 * Declaration for Icon module
 */
declare module '@ticmakers-react-native/icon'

/**
 * Export default
 */
export default Icon
