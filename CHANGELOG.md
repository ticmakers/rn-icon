# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Released

## [1.0.1] - 2019-09-24

## Fixed

- Fix dependencies version

## [1.0.0] - 2019-05-03

### Release

# Unreleased

## [1.0.0-beta.1] - 2019-04-02

### Fixed

- Move declarations to path
- Fix events onLongPress and onPress

## [1.0.0-beta.0] - 2019-04-02

### Added

- Upload and publish first version

[1.0.0]: https://bitbucket.org/ticmakers/rn-icon/src/v1.0.0/
[1.0.0-beta.1]: #
[1.0.0-beta.0]: #
