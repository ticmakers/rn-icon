[@ticmakers-react-native/icon](../README.md) > ["styles"](../modules/_styles_.md)

# External module: "styles"

## Index

### Variables

* [styles](_styles_.md#styles)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  containerStyle: {
  },

  disabledStyle: {
  },

  iconStyle: {
  },
})

*Defined in styles.ts:3*

#### Type declaration

 containerStyle: `object`

 disabledStyle: `object`

 iconStyle: `object`

___

