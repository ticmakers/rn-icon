"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const react_native_elements_1 = require("react-native-elements");
const styles_1 = require("./styles");
class Icon extends React.Component {
    constructor(props) {
        super(props);
        this.state = this._processProps();
    }
    render() {
        const { containerStyle, disabledStyle, iconStyle, onLongPress, onPress } = this._processProps();
        return (React.createElement(react_native_elements_1.Icon, Object.assign({}, this._processProps(), { containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.containerStyle, containerStyle]), disabledStyle: react_native_1.StyleSheet.flatten([styles_1.default.disabledStyle, disabledStyle]), iconStyle: react_native_1.StyleSheet.flatten([styles_1.default.iconStyle, iconStyle]), onLongPress: onLongPress && (() => this._onLongPress()), onPress: onPress && (() => this._onPress()) })));
    }
    _onLongPress() {
        const { onLongPress } = this._processProps();
        if (onLongPress) {
            return onLongPress();
        }
    }
    _onPress() {
        const { onPress } = this._processProps();
        if (onPress) {
            return onPress();
        }
    }
    _processProps() {
        const { color, Component, containerStyle, disabled, disabledStyle, iconStyle, name, onLongPress, onPress, options, raised, reverse, reverseColor, size, type, underlayColor } = this.props;
        const params = {
            Component: (options && options.Component) || (Component || undefined),
            color: (options && options.color) || (color || 'black'),
            containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
            disabled: (options && options.disabled) || (disabled || false),
            disabledStyle: (options && options.disabledStyle) || (disabledStyle || undefined),
            iconStyle: (options && options.iconStyle) || (iconStyle || undefined),
            name: (options && options.name) || (name || ''),
            onLongPress: (options && options.onLongPress) || (onLongPress || undefined),
            onPress: (options && options.onPress) || (onPress || undefined),
            raised: (options && options.raised) || (raised || false),
            reverse: (options && options.reverse) || (reverse || false),
            reverseColor: (options && options.reverseColor) || (reverseColor || 'white'),
            size: (options && options.size) || (size || 26),
            type: (options && options.type) || (type || 'material'),
            underlayColor: (options && options.underlayColor) || (underlayColor || 'black'),
        };
        return params;
    }
}
exports.default = Icon;
//# sourceMappingURL=Icon.js.map