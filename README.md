# TIC Makers - React Native Icon
React native component for icon.

Powered by [TIC Makers](https://ticmakers.com)

## Demo

[Icon Expo's snack]()

## Install

Install `@ticmakers-react-native/icon` package and save into `package.json`:

NPM
```shell
$ npm install @ticmakers-react-native/icon --save
```

Yarn
```shell
$ yarn add @ticmakers-react-native/icon
```

## How to use?

```javascript
import React from 'react'
import { AppLoading, Font } from 'expo'
import Icon from '@ticmakers-react-native/icon'

export default class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isReady: false,
    }
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={ this.loadAssets }
          onFinish={ () => this.setState({ isReady: true }) }
          onError={ console.error }
        />
      )
    }

    return (
      <Icon name="star" />  // <- type by default is material
      // OR
      <Icon name="star" type="font-awesome" />
    )
  }

  async loadAssets() {
    await Font.loadAsync({
      AntDesign: require('@expo/vector-icons/website/src/fonts/AntDesign.ttf'),
      Entypo: require('@expo/vector-icons/website/src/fonts/Entypo.ttf'),
      EvilIcons: require('@expo/vector-icons/website/src/fonts/EvilIcons.ttf'),
      Feather: require('@expo/vector-icons/website/src/fonts/Feather.ttf'),
      FontAwesome: require('@expo/vector-icons/website/src/fonts/FontAwesome.ttf'),
      Foundation: require('@expo/vector-icons/website/src/fonts/Foundation.ttf'),
      Ionicons: require('@expo/vector-icons/website/src/fonts/Ionicons.ttf'),
      MaterialCommunityIcons: require('@expo/vector-icons/website/src/fonts/MaterialCommunityIcons.ttf'),
      MaterialIcons: require('@expo/vector-icons/website/src/fonts/MaterialIcons.ttf'),
      Octicons: require('@expo/vector-icons/website/src/fonts/Octicons.ttf'),
      SimpleLineIcons: require('@expo/vector-icons/website/src/fonts/SimpleLineIcons.ttf'),
      Zocial: require('@expo/vector-icons/website/src/fonts/Zocial.ttf'),
    })
  }
}
```

## Properties

| Name | Type | Default Value | Definition |
| ---- | ---- | ------------- | ---------- |
| name | - | - | -

## Todo

- Test on iOS
- Improve and add new features
- Add more styles
- Improve readme (example & demo)
- Create tests

## Version 1.0.1 ([Changelog])

[Changelog]: https://bitbucket.org/ticmakers/rn-icon/src/master/CHANGELOG.md
